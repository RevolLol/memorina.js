function game(){
    let oTimeMan = oCardMan = null;
    const oStartDlg = $("#startOverlay");


    //handles start dialog options and actions
    function startDlgHandle(){
        //getting either predefined or custom field size
        let nRows = nCols = 0;
        if($("#custSizeEnabled").is(':checked'))
        {
            nRows = $("#numRowsSize option:selected").text();
            nCols = $("#numColsSize option:selected").text();
        }
        else
        {
            [nRows, nCols] = $("#predefSize option:selected").text().split("x");
        }
        //checking if field size is even
        if(!(nRows * nCols % 2))
        {
            oStartDlg.modal("hide");
            $(".main").css("visibility", "visible");
            /**
             * TODO fix hardcoded card set name
             */
            oCardMan = new cardManager(nRows, nCols).setGameEndCb(endGame).makeField($("#selectCardSet").val(), $("#fieldMain"));
            oTimeMan = new timeManager().setTickCallback(nSeconds => $("#timer").text(nSeconds)).startTimer();
        }
        else
        {
            $(".sizeError").css("visibility", "visible");
        }
    }

    /**
     * function is bound to this by default for convenience
     */
    this.startGame = function(){
        oStartDlg.modal('show');
        $("#startButton").click(startDlgHandle);
        $("#restartButton").click(()=>{
            //might be opened from previous game, so force closing it
            $("#endOverlay").modal('hide');
            oStartDlg.modal('show');
        });
        return this;
    }


    function endGame(){
        //stopping the timer
        oTimeMan.stopTimer();
        oCardMan.destroyField();
        //collecting statistics
        const oStats = {
            "Количество секунд с начала игры": oTimeMan.stopTimer(),
            "Общее количество кликов по карточкам": oCardMan.getClicksMade(),
            "Количество неверных сочетаний": oCardMan.getWrongClicks()
        };
        //joining with <br> to exclude trailing line break after last span
        $("#endStats").html(Object.keys(oStats).map(sKey => `<span>${sKey}: ${oStats[sKey]}</span>`).join("<br>"));
        $("#endOverlay").modal('show');
    }
}

function timeManager(nTick = 1000){
    let nInterval = nTimePassed = 0;

    let fTickCb = $.noop;

    this.startTimer = function(){
        nInterval = setInterval(intervalUpd, nTick);
        return this;
    }

    /**
     * counts time passed since the start of the game
     * calls an external function every second
     */
    function intervalUpd(){
        fTickCb(nTimePassed++);
    }    

    this.getCurrTime = function(){
        return nTimePassed;
    }

    //sets external function to be called on each timer tick
    this.setTickCallback = function(fCallback){
        fTickCb = fCallback || fTickCb;
        return this
    }

    this.removeTickCallback = function(){
        fTickCb = $.noop;
        return this;
    }

    this.stopTimer = function(){
        clearInterval(nInterval);
        //removing set callback
        return this.removeTickCallback().getCurrTime();
    }
}

function cardManager(nRows, nCols){
    const nFieldSize = nRows * nCols,
        nMaxCards = 24;
    let oCardOpened = null,
        oFieldRoot = null,
        nClicksMade = nWrongClicks = 0, 
        gameEndCb = $.noop;

    function handleCard(oEvent){
        //counting every click unconditionally
        nClicksMade++;
        const oCard = $(oEvent.currentTarget);
        //if user clicked the same card he opened previously
        if(oCard.hasClass("opened"))
            return closeCard(oCard);
        //otherwise we open the card
        oCard.removeClass("closed").addClass("opened");
        //if no previous card was opened, saving the new one for further operations
        if(!oCardOpened)
        {
            oCardOpened = oCard;
            return true;
        }
        //checking if cards are identical
        if(oCard.css('background-image') === oCardOpened.css('background-image'))
        {
            oCard.addClass("paired");
            oCardOpened.addClass("paired");
        }
        else
        {
            //ensuring user wont open 3-rd card before previous ones close
            oFieldRoot.css("pointer-events", "none");
            setTimeout(closeCard, 1000, [oCard, oCardOpened]);
            nWrongClicks++;
        }
        //removing last opened card
        oCardOpened = null;
        if($(".card.paired").length === nFieldSize)
            gameEndCb();
    }

    function closeCard(vCard){
        vCard = Array.isArray(vCard) ? vCard : [vCard];
        vCard.forEach(oCard => {
            if(oCard instanceof $)
                oCard.removeClass("opened").addClass("closed")
        });
        //enabling pointer-events after cards close
        oFieldRoot.css("pointer-events", "all");
        return true;
    }

    /**
     * upper limit of randomly generated numbers
     * in this case the upper limit is the max number of pics in the set
     * */
    function makeRandArr(nLim){
        const aRands = [];
        //first we generate pairs of random numbers
        while(aRands.length < nFieldSize){
            var nRand = Math.floor(Math.random() * nLim) + 1;
            if(!~aRands.indexOf(nRand)) 
                Array.prototype.push.apply(aRands, [nRand, nRand]);
        }
        //then we shuffle randomly this array
        for (let i = aRands.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [aRands[i], aRands[j]] = [aRands[j], aRands[i]];
        }
        return aRands
    }

    function makeCard(sImgSrc, sCardText="", sClass=""){
        if(!sImgSrc)
            return null;
        const oCard = $( "<div></div>", {
            "class": `card opened ${sClass}`,
            "style": `background-image: url('${sImgSrc}')`,
            on: {
              click: handleCard
            }
        });
        return oCard.append([
            $(`<div class="card-cover-top card-bg">`),
            $(`<div class="card-body card-bg"><p class="card-text">${sCardText}</p></div>`)
        ]);        
    }

    this.makeField = function(sSetName, vElem){
        const sUrl = `res/${sSetName.toLowerCase()}/`,
            aPics = makeRandArr(nMaxCards),
            aCards = [];
        oFieldRoot = vElem instanceof $ ? vElem : $(vElem);
        for(let i = 0; i < nRows; i++)
        {
            const oRow = $('<div class="row row-no-gutters"></div>');
            for(let j= 0; j < nCols; j++)
            {
                let oCard = makeCard(sUrl + `${aPics.pop()}.jpg`, "", "text-center");
                aCards.push(oCard);
                oRow.append($('<div class="col text-center "></div>').append(oCard));
            }
            oFieldRoot.append(oRow);
        }
        setTimeout(closeCard, 3000, aCards);
        return this;
    }

    this.getClicksMade = function(){
        return nClicksMade;
    }

    this.getWrongClicks = function(){
        return nWrongClicks;
    } 

    this.setGameEndCb = function(fCallback){
        gameEndCb = fCallback;
        return this;
    }

    this.clearGameEndCb = function(){
        gameEndCb = $.noop;
        return this;
    }

    this.destroyField = function(){
        oFieldRoot.empty();
    }
}

$(document).ready(function() {
    const oGame = new game().startGame();
});